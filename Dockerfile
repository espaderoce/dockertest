FROM ubuntu:16.04

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . ./

# install packages
RUN apt-get update && \
    apt-get install -y curl \
    wget \
    openjdk-8-jdk
#  # Install Java 11
# RUN apt-get update && \
#     apt-get install -y curl \
#     wget \ openjdk-11-jdk
RUN java /usr/src/app/Test.java > output.txt

# CMD ["java","/usr/src/app/Test.java","> output.txt"]  
# RUN chmod +x /usr/src/app/script.sh 

# COPY output.txt .

# EXPOSE 80
# CMD [ "", "" ]
# ENTRYPOINT "sudo docker cp dockerpr:/usr/src/app/output.txt ."
# CMD ["-f","/dev/null"]