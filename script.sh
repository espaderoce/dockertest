#!/bin/sh
RUN java Test.java>output.txt
docker cp dockerpr:/usr/src/output.txt .
# echo ">>>>  Right before SG initialization <<<<"
# # use while loop to check if elasticsearch is running 
# while true
# do
#     netstat -uplnt | grep :9300 | grep LISTEN > /dev/null
#     verifier=$?
#     if [ 0 = $verifier ]
#         then
#             echo "Running search guard plugin initialization"
#             /elasticsearch/plugins/search-guard-6/tools/sgadmin.sh -h 0.0.0.0 -cd plugins/search-guard-6/sgconfig -icl -key config/client.key -cert config/client.pem -cacert config/root-ca.pem -nhnv
#             break
#         else
#             echo "ES is not running yet"
#             sleep 5
#     fi
# done